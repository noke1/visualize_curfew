# Geo Curfew Visualiser

## __PL__
### 1. Objaśnienie
Aplikacja nie jest idealna i zawiera bledy. Brakuje jej również np. bardziej zaawansowanej walidacji. 
__Niestety na ten moment uważam prace nad nią na zakończone - aplikacja nawet w takiem stanie spełnia swoje zadanie w 100% i zdecydowanie ułatwia pracę. Celem jej powstania było ułatwienie sobie życia i to się udało :)__

### 2. Wprowadzenie
W aplikacji, którą testowałem pojawiła się potrzeba weryfikacji historycznych harmonogramów geograficznych. Były to obszary, w których musial przebywać skazany lub obszary do których skazany nie mógł się zbliżać. Harmonogram geograficzny to nic innego jak obszar na mapie (prostokąt/kwadrat, kolo, poligon).

### 3. Problem
W aplikacji widoczny był tylko aktualnie obowiązujący obszar. Obszary historycznie obowiązujące nie były rysowane na mapie. A czasem pojawiała się potrzeba weryfikacji kszatłtu obszaru, który obowiązywał w przeszłości.

### 4. Rozwiązanie
Obszary rysowane byly na podstawie tego co zostało wyklikane w aplikacji weboej.Rysunek był przekazywany dalej w postaci współrzędnych. Żeby nie rysować tego ręcznie powstała ta aplikacja. W aplikacji należy:
* wybrać kształt obszaru, który chcemy narysować: prostokąt/kwadrat, koło, poligon (do 6 wierzchołków).
* w zależności od wybranego kształtu podać współrzędne wierzchołków (a w przypadku koła równieź promień)
* wpisać nazwę obszaru
* wybrać typ obszaru exclusive/inclusive
Po wypełnieniu pól wybrać przycisk "Draw!". Obszar zostanie narysowany na mapie.

### 5. Jak użyć?
Najłatwiej i najszybciej:
* pobrać repozytorium
* otworzyć projekt w VSC
* zainstalować dodatek "Live Server" w VSC
* kliknąć przycisk "Go Live" w prawym dolnym rogu VSC



## __EN__
### 1. Explanation
The app has bugs and needs to be refactored. It lacks proper validation also.
__But even at this stage my work here is done. Main goal of creating it was to make MY life easier. I know how to use it, my work is faster and life easier :)__


### 2. Summary
I was testing an application for convicts monitoring. There was a possibility to assign a geographical curfew to a convict. This type of curfew is an area (square or rectangle) where:
 * convict has to stay and cannot leave it
 
 or
 * convict cannot be inside of it.
 
 The area was drawn based on curfew shape - rectangle/square, circle, polygon.


### 3. Problem
Only active area was visible inside the app that I was testing. All historical areas were not visible inside the app. Once upon a time a necessity to verify historical area appeard. Drawing it manualy was not so effective...


### 4. Solution
User had to draw shapes with web application. Rectangle/square, circle or polygon was then sent to another system with geo coordinates. I had enough with manually copying and pasting coordinates to some web apps. So I made such application on my own. To draw Geographical Curfew:
* choose the shape that you want to draw - rectangle/square, circle, polygon (up to 6 vertexes)
* pass the coordinates of choosed shape
* pass the curfew name
* choose curfew type - exclusive/inclusive

After specifying those information press "Draw!". The area will be then drawn on the map.

### 5. Usage
Easiest and fastest way: 
* simply clone the repository
* open project via VSC
* instal "Live Server" extension in VSC
* press "Go Live" button in right bottom corner inside VSC
